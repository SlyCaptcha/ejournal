import re
from datetime import datetime

from doc_prefixes import doc_prefix_dict

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseNotFound


def get_new_doc_number(model_name):
    '''
        Возвращает инкрементный номер документа
    '''
    # получение последней последнего объекта-записи из общего списка
    last_record = model_name.objects.last()
    # получение текущего года в краткой форме (последние два знака)
    current_year = datetime.today().strftime('%y')

    if last_record:
        last_number = last_record.number
        # проверка, что введенная строка удовлетворяет шаблону
        checking_string = re.fullmatch(r'^\D+\_\d+\-\w\w$', last_number)
        if checking_string:
            # поиск слова в начале строки
            prefix = re.match(r'^\D+', last_number)
            # поиск числа в строке и инкрементирование на +1
            new_number = int(re.findall(r'\d+', last_number)[0]) + 1
            # возвращение инкрементного номера
            return f'{prefix.group(0).upper()}{new_number}-{current_year}'
    # возвращение первого номера
    return f'{doc_prefix_dict[model_name.__name__]}1-{current_year}'


def new_record(http_request, form_name, url_name):
    '''
        Создание новой записи
    '''
    if http_request.method == "POST":
        form = form_name(http_request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            return redirect(url_name)
    else:
        form = form_name()
    return render(http_request, './form.html', {'form': form})


def edit_record(http_request, id, model_name, form_name, url_name):
    '''
        Редактирование существующей записи
    '''
    record = get_object_or_404(model_name, id=id)
    if http_request.method == "POST":
        form = form_name(http_request.POST, instance=record)
        if form.is_valid():
            data = form.save(commit=False)
            data.save()
            return redirect(url_name)
    else:
        form = form_name(instance=record)
    return render(http_request, './form.html', {'form': form})


def delete_record(id, model_name, url_name):
    '''
        Удаление существующей записи
    '''
    record = get_object_or_404(model_name, id=id)
    if model_name.DoesNotExist:
        return HttpResponseNotFound('Object not found.')
    record.delete()
    return redirect(url_name)
    # try:
    #     # record = model_name.objects.get(id=id)
    #     record = get_object_or_404(model_name, id=id)
    #     record.delete()
    #     return redirect(url_name)
    # except model_name.DoesNotExist:
    #     return HttpResponseNotFound('<h2>Record not found.</h2>')
