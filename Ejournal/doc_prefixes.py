# from ii_acts.models import IncomingInspectionAct
from invoices.models import Invoice
from invoices_fid.models import InvoiceForInternalDisplacement
from purchased_items.models import PurchasedItem
from purchase_requests.models import Request
# from repairs.models import Repair
from specworks.models import SpecialWork


doc_prefix_dict = {
    # IncomingInspectionAct.__name__: 'АКТВХК_',
    Invoice.__name__: 'СЧ_',
    InvoiceForInternalDisplacement.__name__: 'НВП_',
    PurchasedItem: 'ПКИ_',
    Request.__name__: 'ЗНЗ_',
    # Repair.__name__: 'РЕМ_',
    SpecialWork.__name__: 'СР_',
}

NO_RECORDS = 'Список пуст. Добавьте новую запись.'
