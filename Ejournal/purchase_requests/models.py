from django.utils.timezone import now as current_date
from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    IntegerField,
    DateField,
    TextField,
    BooleanField,
    ForeignKey,
    ManyToManyField,
    SET_NULL,
)
from django.contrib.auth.models import User

from projects.models import Project


# Create your models here.
# Тип закупки
class RequestType(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        max_length=50,
        unique=True,
        # verbose_name='title',
        verbose_name='Наименование',
        # error_messages={
        #     'blank': 'Введите корректное наименование!',
        # }
    )

    # Представление в аккаунте администратора
    class Meta:
        ordering = ['id', ]
        # verbose_name_plural = 'Request types'
        verbose_name = 'тип заявки на закупку'
        verbose_name_plural = 'типы заявок на закупку'

    # Отображение в аккаунте администратора
    def __str__(self):
        return f'{self.title}'


# Статус закупки
class RequestStatus(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        max_length=50,
        unique=True,
        # verbose_name='title',
        verbose_name='Наименование',
        # error_messages={
        #     'blank': 'Введите корректное наименование!',
        # }
    )

    # Представление в аккаунте администратора
    class Meta:
        ordering = ['id', ]
        # verbose_name_plural = 'Request types'
        verbose_name = 'статус заявки на закупку'
        verbose_name_plural = 'статусы заявок на закупку'

    # Отображение в аккаунте администратора
    def __str__(self):
        return f'{self.title}'


# Заявка на закупку
class Request(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
        # default=uuid4
    )
    request_type = ForeignKey(
        'RequestType',
        on_delete=SET_NULL,
        null=True,
        verbose_name='Тип заявки',
    )
    number = CharField(
        'Номер',
        # verbose_name='Number',
        max_length=255,
        unique=True,
        # error_messages={
        #     'blank': 'Введите уникальный номер заявки по заданному формату!',
        # }
    )
    created = DateField(
        # auto_now_add=True,
        # verbose_name='Created',
        verbose_name='Дата создания',
        default=current_date,
        null=True,
    )
    appointment_person = ForeignKey(
        User,
        on_delete=SET_NULL,
        # verbose_name='Appointment person',
        verbose_name='Исполнитель',
        blank=True,
        null=True,
    )
    description = TextField(
        # verbose_name='Description',
        verbose_name='Описание',
        blank=True,
        help_text='',
    )
    project = ForeignKey(
        Project,
        on_delete=SET_NULL,
        # verbose_name='Project',
        verbose_name='Проект',
        blank=True,
        null=True,
    )
    status = ForeignKey(
        RequestStatus,
        on_delete=SET_NULL,
        # verbose_name='Request status',
        verbose_name='Статус заявка',
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'заявка на закупку'
        verbose_name_plural = 'заявки на закупку'

    def __str__(self):
        return f'{self.number} ({self.project} - {self.appointment_person})'

    def get_absolute_url(self):
        return reverse('edit_purchase_request', args=(self.id,))
