from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required

from .models import Request
from .forms import PurchaseRequestForm

from general_funcs import new_record, edit_record

APP_NAME = 'purchase_requests'


# purchase_requests
class PurchaseRequstListView(ListView):
    model = Request
    template_name = 'purchase_requests.html'


class PurchaseRequestDetailView(DetailView):
    model = Request
    query_pk_and_slug = True
    slug_field = 'number'
    # slug_url_kwarg = 'PurchaseRequest_number'
    context_object_name = 'record'
    template_name = 'purchase_request_detail.html'


@login_required(login_url='/accounts/login/')
def new_request(request):
    return new_record(request, PurchaseRequestForm, APP_NAME)


@login_required(login_url='/accounts/login/')
def edit_request(request, id):
    return edit_record(request, id, Request, PurchaseRequestForm, APP_NAME)


# @login_required(login_url='/accounts/login/')
# def delete_request(id):
#     return delete_record(id, Request, APP_NAME)
