from django.contrib import admin

from .models import (
    Request,
    RequestType,
    RequestStatus,
)
from supplies.models import Supply


# Register your models here.
# admin.site.register(Request)
# admin.site.register(RequestType)


@admin.register(RequestType)
class RequestTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(RequestStatus)
class RequestStatusAdmin(admin.ModelAdmin):
    pass


class SupplyInline(admin.TabularInline):
    model = Supply


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    inlines = [
        SupplyInline,
    ]


# @admin.register(Request)
# class PurchaseRequestAdmin(admin.ModelAdmin):
#     fields = (
#         'PurchaseRequest_type',
#         'number',
#         'created',
#         'appointment_person',
#         'project',
#         'purchased_item',
#         ('special_check', 'special_studies',),
#         'type_of_quality_control',
#         ('letter_O1', 'qualified_supplier',),
#         'comment',
#         'PurchaseRequest_status',
#     )
#     # inlines = [
#     #     PurchasedItemInline,
#     # ]
#     list_display = (
#         'number',
#         'created',
#         'PurchaseRequest_type',
#         'project',
#         'display_purchased_items',
#         'PurchaseRequest_status',
#     )
#     empty_value_display = '--empty--'
#     list_filter = (
#         'PurchaseRequest_type',
#         'appointment_person',
#         'project',
#         'qualified_supplier',
#         'type_of_quality_control',
#     )
#     search_fields = [
#         'name',
#         'manager',
#     ]
