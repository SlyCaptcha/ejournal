from django.contrib.auth.models import User
from django.forms import (
    ModelForm,
    CharField,
    IntegerField,
    CheckboxSelectMultiple,
    ModelChoiceField,
    ModelMultipleChoiceField,
    DateField,
    BooleanField,

    # windgets
    DateInput,
    TextInput,
    Textarea,
    Select,
    SelectMultiple,
    SelectDateWidget,
)
from datetime import datetime

from .models import Request
# from .models import (
#     Request,
#     RequestType,
#     PurchaseRequestStatus,
#     TypeOfQualityControl,

#     # IncomingInspectionAct,
#     # InvoiceForInternalDisplacement,
# )
# from projects.models import Project
# from purchased_items.models import PurchasedItem
# from invoices.models import Invoice
# from repairs.models import Repair
# from specworks.models import SpecialWork


# from general_funcs import get_new_doc_number as get_new_num


# Вывод ФИО пользователя
# class UserModelChoiceField(ModelChoiceField):
#     def label_from_instance(self, object):
#         return object.get_full_name()


# Автозаполнение номера заявки
# def get_new_number():
#     last_record = Request.objects.last()
#     result = last_record.number
#     return result


class PurchaseRequestForm(ModelForm):
    class Meta:
        model = Request
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     super(PurchaseRequestForm, self).__init__(*args, **kwargs)
    #     self.fields['number'].initial = get_new_num(Request)

    # PurchaseRequest_type = ModelChoiceField(
    #     # label='Request type',
    #     label='Тип заявки на закупку',
    #     queryset=RequestType.objects.all(),
    #     required=True,
    #     initial=1,
    # )
    # number = CharField(
    #     # label='Number',
    #     label='Номер',
    #     required=True,
    #     widget=TextInput(
    #         attrs={
    #             'placeholder': 'ЗНЗ_###-19',
    #         }
    #     ),
    #     error_messages={
    #         'required': 'Введите номер!'
    #         # 'required': 'Enter the number!'
    #     },
    # )
    # created = DateField(
    #     # label='Created',
    #     label='Создано',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # appointment_person = UserModelChoiceField(
    #     # label='Appointment person',
    #     label='Ответственное лицо',
    #     required=False,
    #     queryset=User.objects.all(),
    # )
    # project = ModelChoiceField(
    #     # label='Project',
    #     label='Проект',
    #     required=False,
    #     queryset=Project.objects.all(),
    # )
    # purchased_item = ModelMultipleChoiceField(
    #     # label='Purchase component',
    #     label='Состав',
    #     required=False,
    #     queryset=PurchasedItem.objects.all(),
    #     widget=CheckboxSelectMultiple(),
    # )
    # special_check = BooleanField(
    #     # label='Special check',
    #     label='Специальные проверки (СП)',
    #     required=False,
    #     initial=False,
    # )
    # special_studies = BooleanField(
    #     # label='Special studies',
    #     label='Специальные исследования (СИ)',
    #     required=False,
    #     initial=False,
    # )
    # type_of_quality_control = ModelChoiceField(
    #     # label='Type of quality control',
    #     label='Контроль качества',
    #     required=False,
    #     queryset=TypeOfQualityControl.objects.all(),
    #     initial=1,
    # )
    # letter_O1 = BooleanField(
    #     # label='Letter O1',
    #     label='Наличие литеры O1',
    #     required=False,
    #     initial=False,
    # )
    # qualified_supplier = BooleanField(
    #     # label='Qualified supplier',
    #     label='Квалифицированный поставщик',
    #     required=False,
    #     initial=False,
    # )
    # comment = CharField(
    #     # label='Comment',
    #     label='Комментарий',
    #     required=False,
    #     widget=Textarea(
    #         attrs={
    #             'placeholder': 'Добавьте здесь новый коммернтарий...',
    #         }
    #     ),
    # )
    # PurchaseRequest_status = ModelChoiceField(
    #     # label='PurchaseRequestStatus',
    #     label='Статус заявки',
    #     queryset=PurchaseRequestStatus.objects.all(),
    #     required=True,
    #     initial=1,
    # )
