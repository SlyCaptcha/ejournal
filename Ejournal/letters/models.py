from django.utils.timezone import now as current_date
from django.db import models
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
    ForeignKey,
    SET_NULL,
)

from suppliers.models import Supplier


# Create your models here.
class Letter(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        verbose_name='Тема письма (наименование)',
        # verbose_name='Letter subject (title)',
        max_length=255,
        blank=True,
        null=True,
    )
    date = DateField(
        # auto_now_add=True,
        # verbose_name='Date',
        verbose_name='Дата',
        default=current_date,
        blank=True,
        null=True,
    )
    letter_type = CharField(
        verbose_name='Тип письма',
        # verbose_name='Letter type',
        max_length=255,
        blank=True,
        null=True,
    )
    supplier = ForeignKey(
        Supplier,
        on_delete=SET_NULL,
        # verbose_name='Supplier',
        verbose_name='Поставщик',
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'письмо'
        verbose_name_plural = 'письма'

    def __str__(self):
        return f'{self.title} {self.date}'

    def get_absolute_url(self):
        return reverse('edit_letter', args=(self.id,))
