from django.apps import AppConfig


class LettersConfig(AppConfig):
    name = 'letters'
    verbose_name = 'Письма'
