from django.contrib import admin

from .models import InvoiceForInternalDisplacement as InvFID
from invoices.models import Invoice

# Register your models here.
# admin.site.register(InvFID)


class InvoiceInline(admin.TabularInline):
    model = Invoice


@admin.register(InvFID)
class InvFIDAdmin(admin.ModelAdmin):
    inlines = [
        InvoiceInline,
    ]


# @admin.register(InvoiceForInternalDisplacement)
# class InvoiceForInternalDisplacementAdmin(admin.ModelAdmin):
#     list_display = ('number', 'display_purchased_items', 'created', 'transfered', 'recipient',)
