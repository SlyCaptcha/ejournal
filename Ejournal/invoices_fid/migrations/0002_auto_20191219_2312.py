# Generated by Django 2.2.1 on 2019-12-19 20:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoices_fid', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoiceforinternaldisplacement',
            name='created',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='Дата создания'),
        ),
        migrations.AlterField(
            model_name='invoiceforinternaldisplacement',
            name='transfered',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='Передано'),
        ),
    ]
