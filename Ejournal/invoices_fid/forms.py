from django.contrib.auth.models import User
from django.forms import (
    ModelForm,
    CharField,
    # IntegerField,
    # ModelChoiceField,
    ModelMultipleChoiceField,
    DateField,
    # BooleanField,

    # windgets
    DateInput,
    TextInput,
    # Textarea,
    # Select,
    SelectMultiple,
    SelectDateWidget,
)
from datetime import datetime

from .models import (
    InvoiceForInternalDisplacement,
)

# from purchase_requests.models import PurchasedItem

# from general_funcs import get_new_doc_number as get_new_num


class InvoiceForInternalDisplacementForm(ModelForm):
    class Meta:
        model = InvoiceForInternalDisplacement
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     super(InvoiceForInternalDisplacementForm, self).__init__(*args, **kwargs)
    #     self.fields['number'].initial = get_new_num(InvoiceForInternalDisplacement)

    # number = CharField(
    #     # label='Number:',
    #     label='Номер:',
    #     required=True,
    #     max_length=15,
    #     widget=TextInput(
    #         attrs={
    #             'placeholder': 'НВП19-...',
    #         }
    #     ),
    #     error_messages={
    #         'required': 'Enter the correct number!'
    #     },
    # )
    # purchased_item = ModelMultipleChoiceField(
    #     # label='Purchase component',
    #     label='Состав:',
    #     required=False,
    #     queryset=PurchasedItem.objects.all(),
    #     widget=SelectMultiple(),
    # )
    # created = DateField(
    #     # label='Created:',
    #     label='Создано:',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # transfered = DateField(
    #     # label='Transfered',
    #     label='Передано',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # recipient = CharField(
    #     # label='Recipient:',
    #     label='Получатель:',
    #     required=False,
    #     max_length=50,
    # )
