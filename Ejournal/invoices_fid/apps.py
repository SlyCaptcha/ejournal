from django.apps import AppConfig


class InvoicesFIDConfig(AppConfig):
    name = 'invoices_fid'
    verbose_name = 'Накладные на внутреннее перемещение'
