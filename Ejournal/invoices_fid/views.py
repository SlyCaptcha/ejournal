# from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

from django.views.generic import ListView

from .models import (
    InvoiceForInternalDisplacement as InvoiceFID,
)
from .forms import (
    InvoiceForInternalDisplacementForm as InvoiceFIDForm,
)


# Invoice for internal displacement
class InvoiceFIDListView(ListView):
    model = InvoiceFID
    template_name = 'invoices_fid.html'


@login_required(login_url='/accounts/login/')
def new_invoice_fid(request):
    if request.method == "POST":
        form = InvoiceFIDForm(request.POST)
        if form.is_valid():
            new_record = form.save(commit=False)
            new_record.save()
            form.save_m2m()
            return redirect('invoices_fid')
    else:
        form = InvoiceFIDForm()
    return render(request, 'form.html', {'form': form})


@login_required(login_url='/accounts/login/')
def edit_invoice_fid(request, id):
    editable_record = get_object_or_404(InvoiceFID, id=id)
    if request.method == 'POST':
        form = InvoiceFIDForm(request.POST, instance=editable_record)
        if form.is_valid():
            invoice_fid = form.save(commit=False)
            invoice_fid.save()
            form.save_m2m()
            return redirect('invoices_fid')
    else:
        form = InvoiceFIDForm(instance=editable_record)
    return render(request, 'form.html', {'form': form})
