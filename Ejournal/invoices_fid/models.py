from django.utils.timezone import now as current_date
from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
)

# from purchase_requests.models import PurchasedItem


# Create your models here.
# Накладная на внутреннее перемещение (НВП)
class InvoiceForInternalDisplacement(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    number = CharField(
        'Номер накладной',
        # verbose_name='Number',
        max_length=255,
        help_text='',
        error_messages={
            'blank': 'Введите уникальный и корректный номер накладной!',
        }
    )
    created = DateField(
        # auto_now_add=True,
        # verbose_name='Created',
        verbose_name='Дата создания',
        default=current_date,
        null=True,
    )
    transfered = DateField(
        # auto_now_add=True,
        # verbose_name='Transfered',
        verbose_name='Передано',
        default=current_date,
        null=True,
    )
    recipient = CharField(
        'Получатель',
        # verbose_name='Recipient',
        max_length=50,
        help_text='',
        blank=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'накладная на внутреннее перемещение'
        verbose_name_plural = 'накладные на внутреннее перемещение'

    def __str__(self):
        return f'{self.number}'

    def display_purchased_items(self):
        return ', '.join(
            [purchased_item.title for purchased_item in self.purchased_item.all()]
        )

    def get_absolute_url(self):
        return reverse('edit_invoice_fid', args=(self.id,))
