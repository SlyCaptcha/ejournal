from django.contrib import admin

from .models import Project, ProjectChiefDesigner, ProjectManager
from purchase_requests.models import Request


# Register your models here.
# admin.site.register(Project)
# admin.site.register(ProjectChiefDesigner)
# admin.site.register(ProjectManager)


@admin.register(ProjectChiefDesigner)
class ProjectChiefDesignerAdmin(admin.ModelAdmin):
    pass

@admin.register(ProjectManager)
class ProjectManagerAdmin(admin.ModelAdmin):
    pass

class RequestInline(admin.TabularInline):
    model = Request


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    inlines = [
        RequestInline,
    ]


# @admin.register(Project)
# class Repair(admin.ModelAdmin):
#     list_display = (
#         'title',
#         'decimal_number',
#         'number',
#         'project_manager',
#         'project_chief_designer',
#         'goverment_contract_number',
#         'goverment_contract_date',
#         'goverment_customer',
#         'head_perfomer',
#         )

# @admin.register(ProjectChiefDesigner)
# class ProjectChiefDesigner(admin.ModelAdmin):
#     list_display = (
#         'lastname',
#         'firstname',
#         'patronymic',
#         )

# @admin.register(ProjectManager)
# class ProjectManager(admin.ModelAdmin):
#     list_display = (
#         'lastname',
#         'firstname',
#         'patronymic',
#         )
