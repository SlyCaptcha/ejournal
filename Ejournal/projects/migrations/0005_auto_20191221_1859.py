# Generated by Django 2.2.1 on 2019-12-21 15:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_auto_20191221_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='goverment_contract_date',
            field=models.DateField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Дата ГК'),
        ),
    ]
