# Generated by Django 2.2.1 on 2019-12-19 19:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('purchase_requests', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Developers',
            fields=[
                ('id', models.AutoField(max_length=3, primary_key=True, serialize=False)),
                ('lastname', models.CharField(max_length=50, verbose_name='Фамилия')),
                ('firstname', models.CharField(blank=True, max_length=30, verbose_name='Имя')),
                ('patronymic', models.CharField(blank=True, max_length=30, verbose_name='Отчество')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectChiefDesigner',
            fields=[
                ('developers_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='projects.Developers')),
            ],
            options={
                'verbose_name': 'главный конструктор проекта',
                'verbose_name_plural': 'главные конструктора проектов',
                'ordering': ['id'],
            },
            bases=('projects.developers',),
        ),
        migrations.CreateModel(
            name='ProjectManager',
            fields=[
                ('developers_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='projects.Developers')),
            ],
            options={
                'verbose_name': 'руководитель проекта',
                'verbose_name_plural': 'руководители проектов',
                'ordering': ['id'],
            },
            bases=('projects.developers',),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(max_length=3, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=30, verbose_name='Наименование')),
                ('decimal_number', models.CharField(blank=True, max_length=100, verbose_name='Децимальный номер')),
                ('number', models.CharField(blank=True, max_length=20, verbose_name='Номер проекта')),
                ('goverment_contract_number', models.TextField(blank=True, verbose_name='Номер ГК')),
                ('goverment_contract_date', models.DateField(blank=True, null=True, verbose_name='Дата ГК')),
                ('goverment_customer', models.CharField(blank=True, max_length=100, verbose_name='Заказчик')),
                ('head_perfomer', models.CharField(blank=True, max_length=100, verbose_name='Головной исполнитель')),
                ('purchase_request', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='purchase_requests.Request', verbose_name='Заявка')),
                ('project_chief_designer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectChiefDesigner', verbose_name='Главный конструктор проекта')),
                ('project_manager', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='projects.ProjectManager', verbose_name='Руководитель проекта')),
            ],
            options={
                'verbose_name': 'проект',
                'verbose_name_plural': 'проекты',
                'ordering': ['id'],
            },
        ),
    ]
