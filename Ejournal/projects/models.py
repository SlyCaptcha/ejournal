# from django.utils.timezone import now as current_date
from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    TextField,
    DateField,
    ForeignKey,
    SET_NULL,
)

# from purchase_requests.models import Request


# Create your models here.
class Developers(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
        # default=uuid4
    )
    lastname = CharField(
        max_length=50,
        # verbose_name='Lastname',
        verbose_name='Фамилия',
        # help_text='Фамилия',
    )
    firstname = CharField(
        max_length=30,
        # verbose_name='Firstname',
        verbose_name='Имя',
        # help_text='Имя',
        blank=True
    )
    patronymic = CharField(
        max_length=30,
        # verbose_name='Patronymic',
        verbose_name='Отчество',
        # help_text='Отчество',
        blank=True
    )

    def __str__(self):
        return f'{self.lastname} {self.firstname} {self.patronymic}'


# Руководитель проекта
class ProjectManager(Developers):
    # id = AutoField(
    #     primary_key=True,
    #     max_length=3,
    #     # default=uuid4
    # )
    # lastname = CharField(
    #     max_length=50,
    #     # verbose_name='Lastname',
    #     verbose_name='Фамилия',
    #     # help_text='Фамилия',
    # )
    # firstname = CharField(
    #     max_length=30,
    #     # verbose_name='Firstname',
    #     verbose_name='Имя',
    #     # help_text='Имя',
    #     blank=True
    # )
    # patronymic = CharField(
    #     max_length=30,
    #     # verbose_name='Patronymic',
    #     verbose_name='Отчество',
    #     # help_text='Отчество',
    #     blank=True
    # )

    # class Meta:
    #     ordering = ['id', ]
    #     verbose_name = 'руководитель проекта'
    #     verbose_name_plural = 'руководители проектов'

    # def __str__(self):
    #     return f'{self.lastname} {self.firstname} {self.patronymic}'

    class Meta:
        ordering = ['id', ]
        verbose_name = 'руководитель проекта'
        verbose_name_plural = 'руководители проектов'


# Главный конструктор проекта
class ProjectChiefDesigner(Developers):
    # id = AutoField(
    #     primary_key=True,
    #     max_length=3,
    #     # default=uuid4
    # )
    # lastname = CharField(
    #     max_length=50,
    #     # verbose_name='Lastname',
    #     verbose_name='Фамилия',
    #     # help_text='Фамилия',
    # )
    # firstname = CharField(
    #     max_length=30,
    #     # verbose_name='Firstname',
    #     verbose_name='Имя',
    #     # help_text='Имя',
    #     blank=True
    # )
    # patronymic = CharField(
    #     max_length=30,
    #     # verbose_name='Patronymic',
    #     verbose_name='Отчество',
    #     # help_text='Отчество',
    #     blank=True
    # )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'главный конструктор проекта'
        verbose_name_plural = 'главные конструктора проектов'

    # def __str__(self):
    #     return f'{self.lastname} {self.firstname} {self.patronymic}'


# Проекты
class Project(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        max_length=30,
        # unique=True,
        # verbose_name='Title',
        verbose_name='Наименование',
        # help_text='Шифр'
    )
    decimal_number = CharField(
        max_length=100,
        # verbose_name='Decimal number',
        verbose_name='Децимальный номер',
        # help_text='Децимальный номер',
        blank=True,
    )
    number = CharField(
        max_length=20,
        # verbose_name='Number',
        verbose_name='Номер проекта',
        # help_text='№ проекта',
        blank=True,
    )
    project_manager = ForeignKey(
        ProjectManager,
        on_delete=SET_NULL,
        null=True,
        # verbose_name='Project manager',
        verbose_name='Руководитель проекта',
        # help_text='РП',
        blank=True,
    )
    project_chief_designer = ForeignKey(
        ProjectChiefDesigner,
        on_delete=SET_NULL,
        null=True,
        # verbose_name='Project chief designer',
        verbose_name='Главный конструктор проекта',
        # help_text='ГКП',
        blank=True,
    )
    goverment_contract_number = TextField(
        # verbose_name='Goverment contract number',
        verbose_name='Номер ГК',
        # help_text='№ ГК',
        blank=True,
    )
    goverment_contract_date = DateField(
        # auto_now_add=True,
        # verbose_name='Goverment contract date',
        verbose_name='Дата ГК',
        # default=current_date,
        blank=True,
        null=True,
    )
    goverment_customer = CharField(
        max_length=100,
        # verbose_name='Goverment customer',
        verbose_name='Заказчик',
        # help_text='Заказчик',
        blank=True,
    )
    head_perfomer = CharField(
        max_length=100,
        # verbose_name='Head perfomer',
        verbose_name='Головной исполнитель',
        # help_text='Головной исполнитель',
        blank=True,
    )
    # purchase_request = ForeignKey(
    #     Request,
    #     on_delete=SET_NULL,
    #     null=True,
    #     # verbose_name='Purchase request',
    #     verbose_name='Заявка',
    #     # help_text='Заявка',
    #     blank=True,
    # )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'проект'
        verbose_name_plural = 'проекты'

    def __str__(self):
        return f'{self.number} {self.title}'

    def get_absolute_url(self):
        return reverse('edit_project', args=(self.id,))
