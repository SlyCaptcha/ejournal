from django.views.generic import ListView
from django.contrib.auth.decorators import login_required

from .models import Project
from .forms import ProjectsForm

from general_funcs import new_record, edit_record

APP_NAME = 'projects'


# Create your views here.
class ProjectsList(ListView):
    model = Project
    template_name = 'projects.html'


@login_required(login_url='/accounts/login/')
def new_project(request):
    return new_record(request, ProjectsForm, APP_NAME)


@login_required(login_url='/accounts/login/')
def edit_project(request, id):
    return edit_record(request, id, Project, ProjectsForm, APP_NAME)


# @login_required(login_url='/accfounts/login/')
# def new_project(request):
#     if request.method == "POST":
#         form = ProjectsForm(request.POST)
#         if form.is_valid():
#             project = form.save(commit=False)
#             project.save()
#             return redirect('projects')
#     else:
#         form = ProjectsForm()
#     return render(request, './form.html', {'form': form})


# @login_required(login_url='/accfounts/login/')
# def edit_project(request, id):
#     record = get_object_or_404(Project, id=id)
#     if request.method == "POST":
#         form = ProjectsForm(request.POST, instance=record)
#         if form.is_valid():
#             project = form.save(commit=False)
#             project.save()
#             return redirect('projects')
#     else:
#         form = ProjectsForm(instance=record)
#     return render(request, './form.html', {'form': form})
