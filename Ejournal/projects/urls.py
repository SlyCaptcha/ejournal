"""ejournal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from projects.views import ProjectsList, new_project, edit_project


urlpatterns = [
    url(r'^$', ProjectsList.as_view(), name='projects'),
    # url(r'^(?P<slug>[-\w]+)/detail$', PurchaseDetailView.as_view(), name='purchase_detail'),
    url(r'^new$', new_project, name='new_project'),
    url(r'^(?P<id>\d+)/edit$', edit_project, name='edit_project'),
]
