from datetime import datetime
from django.forms import (
    ModelForm,
    ModelChoiceField,
    CharField,
    Textarea,
    DateField,
    SelectDateWidget,

    TextInput,
    DateInput,
)

from .models import ProjectManager, ProjectChiefDesigner, Project


class ProjectsForm(ModelForm):
    class Meta:
        model = Project
        fields = '__all__'
    
    # title = CharField(
    #     label='Шифр:',
    #     required=True,
    #     max_length=30,
    # )
    # decimal_number = CharField(
    #     label='Децимальный номер:',
    #     required=False,
    #     max_length=100,
    # )
    # number = CharField(
    #     label='№ проекта:',
    #     required=False,
    #     max_length=20,
    # )
    # project_manager = ModelChoiceField(
    #     label='РП:',
    #     queryset=ProjectManager.objects.all(),
    #     required=False,
    # )
    # project_chief_designer = ModelChoiceField(
    #     label='ГКП:',
    #     queryset=ProjectChiefDesigner.objects.all(),
    #     required=False,
    # )
    # goverment_contract_number = CharField(
    #     label='№ ГК',
    #     required=False,
    #     widget=TextInput(),
    # )
    # goverment_contract_date = DateField(
    #     label='Дата ГК',
    #     required=False,
    #     widget=SelectDateWidget(), 
    #     initial=datetime.now().date(),
    # )
    # goverment_customer = CharField(
    #     label='Заказчик',
    #     required=False,
    #     max_length=100,
    # )
    # head_perfomer = CharField(
    #     label='Головной исполнитель',
    #     required=False,
    #     max_length=100,
    # )
