import re
from datetime import datetime


ERROR_MESSAGE = 'Введите корректный номер документа!'


while 1:
    # ввод строки
    last_number = input('Введите номер документа: ')
    # проверка, что строка не пустая
    if last_number:
        # проверка, что не введен спецсимвол
        if last_number != 'q':
            # проверка, что введенная строка удовлетворяет шаблону
            checking_string = re.fullmatch(r'^\D+\_\d+\-\w\w$', last_number)
            if not checking_string:
                print(ERROR_MESSAGE)
                continue

            # поиск слова в начале строки
            prefix = re.match(r'^\D+', last_number)
            # поиск числа в строке и инкрементирование на 1
            new_number = int(re.findall(r'\d+', last_number)[0]) + 1
            # получение текущего года в краткой форме
            current_year = datetime.today().strftime('%y')

            # checking
            print(
                'Проверка:',
                # 'YES' if checking_string else 'NO', 
                checking_string,
                prefix.group(0), new_number, 
                sep='\n'
            )

            print(f'Результат:\n{prefix.group(0).upper()}{new_number}-{current_year}' if prefix and new_number else 'Not found...')
            continue
        print('Выход...')
        break

    print(ERROR_MESSAGE)
    continue


def get_new_doc_number(model_name):
    last_record = model_name.objects.last()
    last_number = str(0)

    if last_record:
        last_number = last_record.number

    #checking
    print(last_number)

    if last_number:
        # проверка, что введенная строка удовлетворяет шаблону
        checking_string = re.fullmatch(r'^\D+\_\d+\-\w\w$', last_number)
        if checking_string:
            # поиск слова в начале строки
            prefix = re.match(r'^\D+', last_number)
            # поиск числа в строке и инкрементирование на 1
            new_number = int(re.findall(r'\d+', last_number)[0]) + 1
            # получение текущего года в краткой форме
            current_year = datetime.today().strftime('%y')

            result = f'{prefix.group(0).upper()}{new_number}-{current_year}'
            return result
