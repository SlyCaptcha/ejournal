from django.utils.timezone import now as current_date
from django.db import models
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
)


# Create your models here.
class IncomeOrder(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    number = CharField(
        verbose_name='Номер документа',
        # verbose_name='Number',
        max_length=255,
        blank=True,
        null=True,
    )
    created = DateField(
        # auto_now_add=True,
        # verbose_name='Number',
        verbose_name='Номер документа',
        default=current_date,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'приходный ордер'
        verbose_name_plural = 'приходные ордера'

    def __str__(self):
        return f'{self.number} {self.created}'

    def get_absolute_url(self):
        return reverse('edit_income_order', args=(self.id,))
