from django.contrib import admin

from .models import IncomeOrder

# Register your models here.
admin.site.register(IncomeOrder)
