from django.apps import AppConfig


class IncomeOrdersConfig(AppConfig):
    name = 'income_orders'
    verbose_name = 'Приходные ордера'
