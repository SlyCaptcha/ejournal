from django.utils.timezone import now as current_date
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
    ForeignKey,
    SET_NULL
)
from django.contrib.auth.models import User

# from invoices.models import Invoice
from suppliers.models import Supplier
from purchase_requests.models import Request


# Create your models here.
class DocType(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        verbose_name='Наименование',
        # verbose_name='Title',
        max_length=255,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'тип документа'
        verbose_name_plural = 'типы документов'

    def __str__(self):
        return f'{self.title}'


class SupplyStatuses(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    title = CharField(
        verbose_name='Наименование',
        # verbose_name='Title',
        max_length=255,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'статус поставки'
        verbose_name_plural = 'статусы поставки'

    def __str__(self):
        return f'{self.title}'


class Supply(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    doc_type = ForeignKey(
        'DocType',
        on_delete=SET_NULL,
        null=True,
        blank=True,
        verbose_name='Тип документа',
    )
    doc_number = CharField(
        verbose_name='Номер документ',
        # verbose_name='Document number',
        max_length=255,
        blank=True,
        null=True,
    )
    doc_date = DateField(
        # auto_now_add=True,
        verbose_name='Дата документа',
        # verbose_name='Document date',
        default=current_date,
        blank=True,
        null=True,
    )
    purchase_request = ForeignKey(
        Request,
        on_delete=SET_NULL,
        # verbose_name='Request',
        verbose_name='Заявка',
        blank=True,
        null=True,
    )
    delivery_time = CharField(
        verbose_name='Срок поставки',
        # verbose_name='Delivery time',
        max_length=255,
        blank=True,
        null=True,
    )
    comment = CharField(
        verbose_name='Комментарий',
        # verbose_name='Comment',
        max_length=255,
        blank=True,
        null=True,
    )
    status = ForeignKey(
        'SupplyStatuses',
        on_delete=SET_NULL,
        # verbose_name='Status',
        verbose_name='Статус',
        blank=True,
        null=True,
    )
    supplier = ForeignKey(
        Supplier,
        on_delete=SET_NULL,
        # verbose_name='Supplier',
        verbose_name='Поставщик',
        blank=True,
        null=True,
    )
    appointment_person = ForeignKey(
        User,
        on_delete=SET_NULL,
        # verbose_name='Appointment person',
        verbose_name='Исполнитель',
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'поставка'
        verbose_name_plural = 'поставки'

    def __str__(self):
        return f'{self.purchase_request}: {self.supplier}'

    def get_absolute_url(self):
        return reverse('edit_supply', args=(self.id,))
