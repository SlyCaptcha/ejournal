from django.contrib import admin

from .models import Supply, DocType, SupplyStatuses
from invoices.models import Invoice

# Register your models here.
# admin.site.register(Supply)
# admin.site.register(DocType)
# admin.site.register(SupplyStatuses)


@admin.register(DocType)
class DocTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(SupplyStatuses)
class SupplyStatusesAdmin(admin.ModelAdmin):
    pass


class InvoiceInline(admin.TabularInline):
    model = Invoice


@admin.register(Supply)
class SupplyAdmin(admin.ModelAdmin):
    inlines = [
        InvoiceInline,
    ]
