"""ejournal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from suppliers.views import SuppliersListView, new_supplier, edit_supplier

urlpatterns = [
    url(r'^$', SuppliersListView.as_view(), name='suppliers'),
    # url(r'^(?P<slug>[-\w]+)/detail$', PurchaseDetailView.as_view(), name='purchase_detail'),
    url(r'^new$', new_supplier, name="new_supplier"),
    url(r'^(?P<id>\d+)/edit$', edit_supplier, name="edit_supplier"),
]
