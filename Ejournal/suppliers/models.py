from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
    TextField,
    EmailField,
    BooleanField,
    ForeignKey,
    SET_NULL,
)

# from supplies.models import Supply
# from purchase_requests.models import Request
# from letters.models import Letter


# Поставщики
class Supplier(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    name = CharField(
        max_length=100,
        # verbose_name='Company name',
        verbose_name='Наименование',
        # help_text='Краткое наименование организации',
    )
    tax_number_id = CharField(
        max_length=8,
        unique=True,
        # verbose_name='Tax number id',
        verbose_name='ИНН',
    )
    ogrn = CharField(
        max_length=13,
        unique=True,
        # verbose_name='OGRN',
        verbose_name='ОГРН',
        blank=True,
        null=True,

    )
    legal_address = CharField(
        # verbose_name='Legal address',
        verbose_name='Юр. адрес',
        max_length=255,
        blank=True,
        null=True,
    )
    actual_address = CharField(
        # verbose_name='Actual address',
        verbose_name='Факт. адрес',
        max_length=255,
        blank=True,
        null=True,
    )
    manager = CharField(
        # verbose_name='Manager\'s name',
        verbose_name='Менеджер',
        max_length=255,
        blank=True,
    )
    email = EmailField(
        # verbose_name='Manager\'s email',
        verbose_name='E-mail',
        max_length=255,
        blank=True,
    )
    phone = CharField(
        # verbose_name='Manager\'s phone number',
        verbose_name='Телефон',
        max_length=100,
        blank=True,
        # help_text='(000)000-00-00'
    )
    iso9001 = BooleanField(
        # verbose_name='ISO 9001',
        verbose_name='ISO 9001',
        default=False,
        blank=True,
    )
    gost_rv = BooleanField(
        # verbose_name='GOST RV 0015-002-2012',
        verbose_name='ГОСТ РВ 0015-002-2012',
        default=False,
        blank=True,
    )
    military_acceptance = BooleanField(
        # verbose_name='Military acceptance',
        verbose_name='Военная приемка',
        default=False,
        blank=True,
    )
    # letter = ForeignKey(
    #     Letter,
    #     on_delete=SET_NULL,
    #     null=True,
    #     blank=True,
    #     verbose_name='Письмо',
    # )
    # supply = CharField(
    #     verbose_name='Поставка',
    #     max_length=255,
    #     blank=True,
    #     null=True,
    # )
    # purchase_request = ForeignKey(
    #     Request,
    #     on_delete=SET_NULL,
    #     # verbose_name='Request',
    #     verbose_name='Заявка',
    #     blank=True,
    #     null=True,
    # )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'поставщик'
        verbose_name_plural = 'поставщики'

    def __str__(self):
        return f'{self.name}'

    def get_absolute_url(self):
        return reverse('edit_supplier', args=(self.id,))
