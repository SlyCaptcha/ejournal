from django.apps import AppConfig


class SuppliersConfig(AppConfig):
    name = 'suppliers'
    verbose_name = 'Поставщики'
