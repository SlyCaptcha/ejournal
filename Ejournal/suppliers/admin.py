from django.contrib import admin

from .models import Supplier
from supplies.models import Supply
from letters.models import Letter


# Register your models here.
# admin.site.register(Supplier)


class SupplyInline(admin.TabularInline):
    model = Supply


class LetterInline(admin.TabularInline):
    model = Letter


@admin.register(Supplier)
class SupplierAdmin(admin.ModelAdmin):
    inlines = [
        SupplyInline,
        LetterInline,
    ]


# @admin.register(Supplier)
# class SupplierAdmin(admin.ModelAdmin):
#     pass

    # fields = (
    #     'name',
    #     'tax_number_id',
    #     'legal_address',
    #     'actual_address',
    #     'manager',
    #     'email',
    #     'phone',
    #     ('iso9001', 'gost_rv',),
    # )
    # # fixme: Это заполнитель пустых значений в строках. Почему он не работает?
    # empty_value_display = '-empty-'
    # list_display = (
    #     'name',
    #     'tax_number_id',
    #     'legal_address',
    #     'actual_address',
    #     'manager',
    #     'email',
    #     'phone',
    #     'iso9001',
    #     'gost_rv',
    # )
    # list_filter = (
    #     'iso9001',
    #     'gost_rv',
    # )
    # search_fields = [
    #     'name',
    #     'manager',
    # ]
    
    # # def __init__(self, *args, **kwargs):
    # #     super().__init__(*args, **kwargs)
    # #     self.fields['legal_address'].widget.attrs['placeholder'] = ('JOPA',)
    # # formfield_overrides = {
    # #     Supplier.actual_address: {
    # #         'widget': TextInput(attrs={
    # #             'placeholder': 'jopa',
    # #         })
    # #     }
    # # }
