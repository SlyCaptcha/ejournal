from django.forms import (
    ModelForm,
    CharField,
    BooleanField,

    Textarea,
    TextInput,
)

from .models import Supplier


class SupplierForm(ModelForm):
    class Meta:
        model = Supplier
        fields = '__all__'

    # name = CharField(
    #     label='Наименование:',
    #     required=True,
    #     max_length=100,
    #     widget=TextInput(attrs={
    #         'placeholder': 'Краткое наименование организации'
    #     })
    # )
    # tax_number_id = CharField(
    #     label='ИНН:',
    #     required=True,
    #     max_length=8,
    # )
    # legal_address = CharField(
    #     label='Юр. адрес:',
    #     required=False,
    #     widget=Textarea,
    # )
    # actual_address = CharField(
    #     label='Факт. адрес:',
    #     required=False,
    #     widget=Textarea,
    # )
    # manager = CharField(
    #     label='Менеджер:',
    #     required=False,
    #     max_length=150,
    # )
    # email = CharField(
    #     label='E-mail:',
    #     required=False,
    #     max_length=100,
    # )
    # phone = CharField(
    #     label='Телефон:',
    #     required=False,
    #     max_length=100,
    #     widget=TextInput(attrs={
    #         'placeholder': '8(000)000-00-00'
    #     })
    # )
    # iso9001 = BooleanField(
    #     label='ISO:',
    #     required=False,
    #     initial=False

    # )
    # gost_rv = BooleanField(
    #     label='ГОСТ РВ 0015-002-2012:',
    #     required=False,
    #     initial=False

    # )
