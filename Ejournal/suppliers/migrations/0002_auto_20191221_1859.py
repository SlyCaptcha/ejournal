# Generated by Django 2.2.1 on 2019-12-21 15:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchase_requests', '0005_remove_request_supplier'),
        ('suppliers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='supplier',
            name='supply',
        ),
        migrations.AddField(
            model_name='supplier',
            name='purchase_request',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='purchase_requests.Request', verbose_name='Заявка'),
        ),
    ]
