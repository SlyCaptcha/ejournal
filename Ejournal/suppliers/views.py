from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView

from .models import Supplier
from .forms import SupplierForm


# Create your views here.
class SuppliersListView(ListView):
    model = Supplier
    template_name = 'suppliers.html'


@login_required(login_url='/accounts/login/')
def new_supplier(request):
    if request.method == "POST":
        form = SupplierForm(request.POST)
        if form.is_valid():
            supplier = form.save(commit=False)
            # todo: Добавить валидацию

            supplier.save()
            return redirect('suppliers')
    else:
        form = SupplierForm()
    return render(request, './form.html', {'form': form})

@login_required(login_url='/accounts/login/')
def edit_supplier(request, id):
    record = get_object_or_404(Supplier, id=id)
    if request.method == "POST":
        form = SupplierForm(request.POST, instance=record)
        if form.is_valid():
            supplier = form.save(commit=False)
            # todo: Добавить валидацию
            
            supplier.save()
            return redirect('suppliers')
    else:
        form = SupplierForm(instance=record)
    return render(request, './form.html', {'form': form})
