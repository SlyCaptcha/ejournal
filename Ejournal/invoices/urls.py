"""ejournal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from invoices.views import InvoiceListView, new_invoice, edit_invoice

urlpatterns = [
    url(r'^$', InvoiceListView.as_view(), name='invoices'),
    url(r'^new$', new_invoice, name="new_invoice"),
    url(r'^(?P<id>[-\d]+)/edit$', edit_invoice, name="edit_invoice"),
]
