from django.utils.timezone import now as current_date
from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DateField,
    DecimalField,
    ForeignKey,
    ManyToManyField,
    SET_NULL,
)
from django.contrib.auth.models import User

# from purchased_items.models import PurchasedItem
# from suppliers.models import Supplier
from supplies.models import Supply
# from closing_docs.models import ClosingDoc
from invoices_fid.models import InvoiceForInternalDisplacement


# Create your models here.
class Invoice(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    number = CharField(
        max_length=255,
        # verbose_name='Number',
        verbose_name='Номер',
        help_text=''
    )
    # supplier = ForeignKey(
    #     Supplier,
    #     on_delete=SET_NULL,
    #     null=True,
    #     blank=True,
    #     # verbose_name='Supplier',
    #     verbose_name='Поставщик',
    #     help_text='',
    # )
    invoice_number = CharField(
        max_length=255,
        # verbose_name='Invoice number',
        verbose_name='Номер счета',
        help_text=''
    )
    date = DateField(
        # verbose_name='Invoice date',
        verbose_name='Дата счета',
        default=current_date,
        blank=True,
        null=True,
    )
    supply = ForeignKey(
        Supply,
        on_delete=SET_NULL,
        # verbose_name='Supply',
        verbose_name='Поставка',
        blank=True,
        null=True,
    )
    signed = DateField(
        # auto_now_add=True,
        # verbose_name='Signed',
        verbose_name='Подписан в оплату',
        default=current_date,
        blank=True,
        null=True,
    )
    payment_day = DateField(
        # auto_now_add=True,
        # verbose_name='Payment day',
        verbose_name='Оплачен',
        default=current_date,
        blank=True,
        null=True,
    )
    amount = DecimalField(
        # verbose_name='Amount',
        verbose_name='Сумма к оплате',
        max_digits=12,
        decimal_places=2,
        default=0,
        blank=True,
    )
    appointment_person = ForeignKey(
        User,
        on_delete=SET_NULL,
        null=True,
        blank=True,
        # verbose_name='Appointment person',
        verbose_name='Ответственное лицо',
        help_text='',
    )
    # fixme: Foreign key?
    # purchased_item = ForeignKey(
    #     PurchasedItem,
    #     on_delete=SET_NULL,
    #     # verbose_name='Purchased item',
    #     verbose_name='ПКИ',
    #     blank=True,
    #     null=True,
    #     help_text='',
    # )
    # ManyToManyField(
    #     PurchasedItem,
    #     blank=True,
    #     verbose_name='Состав',
    #     help_text='',
    # )
    # closing_doc = ForeignKey(
    #     ClosingDoc,
    #     on_delete=SET_NULL,
    #     # verbose_name='Closing documents',
    #     verbose_name='ТН (УПД)',
    #     blank=True,
    #     null=True,
    # )
    invoice_for_internal_displacement = ForeignKey(
        InvoiceForInternalDisplacement,
        on_delete=SET_NULL,
        # verbose_name='Invoice for internal displacement',
        verbose_name='Накладная на внутреннее перемещение (НВП)',
        blank=True,
        null=True,
        help_text='',
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'счет'
        verbose_name_plural = 'счета'

    def __str__(self):
        return f'{self.number}: {self.invoice_number} от {self.date} - {self.appointment_person}'

    def get_absolute_url(self):
        return reverse('edit_invoice', args=(self.id,))
