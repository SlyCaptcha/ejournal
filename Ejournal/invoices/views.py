from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

from django.views.generic import ListView

from .models import Invoice
from .forms import InvoiceForm


# purchase_requests
class InvoiceListView(ListView):
    model = Invoice
    template_name = 'invoices.html'


@login_required(login_url='/accounts/login/')
def new_invoice(request):
    """
    Добавление нового счета
    """
    if request.method == "POST":
        form = InvoiceForm(request.POST)
        if form.is_valid():
            new_record = form.save(commit=False)
            new_record.save()
            form.save_m2m()
            return redirect('invoices')
    else:
        form = InvoiceForm()
    return render(request, 'form.html', {'form': form})


@login_required(login_url='/accounts/login/')
def edit_invoice(request, id):
    """
    Редактирование счета
    """
    editable_record = get_object_or_404(Invoice, id=id)
    if request.method == 'POST':
        form = InvoiceForm(request.POST, instance=editable_record)
        if form.is_valid():
            invoice = form.save(commit=False)
            invoice.save()
            form.save_m2m()
            return redirect('invoices')
    else:
        form = InvoiceForm(instance=editable_record)
    return render(request, 'form.html', {'form': form})
