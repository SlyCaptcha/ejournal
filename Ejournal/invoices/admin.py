from django.contrib import admin

from .models import Invoice
from closing_docs.models import ClosingDoc
from purchased_items.models import PurchasedItem


# Register your models here.
# admin.site.register(Invoice)


class ClosingDocInline(admin.TabularInline):
    model = ClosingDoc


class PurchasedItemInline(admin.TabularInline):
    model = PurchasedItem


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    inlines = [
        ClosingDocInline,
        PurchasedItemInline,
    ]
