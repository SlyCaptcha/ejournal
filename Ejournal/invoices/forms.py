from django.contrib.auth.models import User
from django.forms import (
    ModelForm,
    CharField,
    IntegerField,
    DecimalField,
    ModelChoiceField,
    ModelMultipleChoiceField,
    DateField,
    BooleanField,

    # windgets
    DateInput,
    TextInput,
    Textarea,
    Select,
    SelectMultiple,
    SelectDateWidget,
)
from datetime import datetime

from .models import Invoice
from purchase_requests.models import Request
from purchased_items.models import PurchasedItem
from suppliers.models import Supplier

from general_funcs import get_new_doc_number as get_new_num


# Custom classes
class UserModelChoiceField(ModelChoiceField):
    def label_from_instance(self, object):
        return object.get_full_name()


class InvoiceForm(ModelForm):
    class Meta:
        model = Invoice
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     super(InvoiceForm, self).__init__(*args, **kwargs)
    #     self.fields['number'].initial = get_new_num(Invoice)

    # number = CharField(
    #     # label='Number:',
    #     label='Номер:',
    #     required=True,
    #     widget=TextInput(
    #         attrs={
    #             'placeholder': 'З###-19',
    #         }
    #     ),
    # )
    # Request = ModelChoiceField(
    #     # label='Supplier:',
    #     label='Заявка на закупку:',
    #     required=False,
    #     queryset=Request.objects.all(),
    #     help_text='Выбирете заявку на закупку',
    # )
    # supplier = ModelChoiceField(
    #     # label='Supplier:',
    #     label='Поставщик:',
    #     required=False,
    #     queryset=Supplier.objects.all(),
    #     help_text='Выбирете поставщика',
    # )
    # invoice_number = CharField(
    #     # label='Invoice number:',
    #     label='Номер счета:',
    #     required=True,
    #     widget=TextInput(
    #         attrs={
    #             'placeholder': 'Введите номер счета',
    #         }
    #     ),
    # )
    # invoice_date = DateField(
    #     # label='Invoice date:',
    #     label='Дата счета:',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # signed = DateField(
    #     # label='Signed',
    #     label='Подписан в оплату:',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # payment_day = DateField(
    #     # label='Payment day',
    #     label='Оплачен:',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # purchased_item = ModelMultipleChoiceField(
    #     label='Состав',
    #     required=False,
    #     queryset=PurchasedItem.objects.all(),
    #     widget=SelectMultiple(
    #         attrs={
    #             'placeholder': 'Выбирете состав',
    #         }
    #     ),
    # )
    # amount = DecimalField(
    #     # label='Amount',
    #     label='Сумма к оплате',
    #     required=False,
    #     widget=TextInput(
    #         attrs={
    #             'placeholder': 'Введите сумму',
    #         }
    #     ),
    # )
    # # amount = FloatField(
    # #     # max_length=100,
    # #     # label='Amount',
    # #     label='Сумма к оплате',
    # #     required=False,
    # #     initial=0.00,
    # #     widget=TextInput(
    # #         attrs={
    # #             'placeholder': 'Введите сумму',
    # #         }
    # #     ),
    # # )
    # appointment_person = UserModelChoiceField(
    #     # label='Appointment person:',
    #     label='Ответственное лицо:',
    #     required=False,
    #     queryset=User.objects.all(),
    #     help_text='Выбирете сотрудника',
    # )
