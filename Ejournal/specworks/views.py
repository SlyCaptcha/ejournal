from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from django.views.generic import ListView

from .models import SpecialWork
from .forms import SpecialWorksForm


# Create your views here.
class SpecWorksList(ListView):
    model = SpecialWork
    template_name = 'specworks.html'

@login_required(login_url='/accounts/login/')
def new_specwork(request):
    if request.method == "POST":
        form = SpecialWorksForm(request.POST)
        if form.is_valid():
            specwork = form.save(commit=False)
            specwork.save()
            return redirect('specworks')
    else:
        form = SpecialWorksForm()
    return render(request, 'form.html', {'form': form})

@login_required(login_url='/accounts/login/')
def edit_specwork(request, id):
    record = get_object_or_404(SpecialWork, id=id)
    if request.method == "POST":
        form = SpecialWorksForm(request.POST, instance=record)
        if form.is_valid():
            specwork = form.save(commit=False)
            specwork.save()
            return redirect('specworks')
    else:
        form = SpecialWorksForm(instance=record)
    return render(request, 'form.html', {'form': form})

    