from datetime import datetime
from django.forms import (
    ModelForm,
    CharField,
    DateField,
    Textarea,
    SelectDateWidget,

    TextInput,
    DateInput,
)

from .models import SpecialWork

from general_funcs import get_new_doc_number as get_new_num


class SpecialWorksForm(ModelForm):
    class Meta:
        model = SpecialWork
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SpecialWorksForm, self).__init__(*args, **kwargs)
        self.fields['number'].initial = get_new_num(SpecialWork)

    number = CharField(
        label='Номер:',
        required=True,
        max_length=10,
    )
    created = DateField(
        label='Дата:',
        required=False,
        widget=SelectDateWidget(),
        initial=datetime.now().date(),
    )
    Request = CharField(
        label='Заявка:',
        required=False,
        max_length=10,
    )
    executive_company = CharField(
        label='Контрагент:',
        required=False,
        max_length=50,
    )
    output_number = CharField(
        label='Исходящий №:',
        required=False,
        max_length=50,
    )
    intput_number = CharField(
        label='Входящий №:',
        required=False,
        max_length=50,
    )
    conclusion = CharField(
        label='Заключение:',
        required=False,
        max_length=50,
    )
    protocol = CharField(
        label='Протокол:',
        required=False,
        max_length=50,
    )
    prescription = CharField(
        label='Предписание:',
        required=False,
        max_length=50,
    )
