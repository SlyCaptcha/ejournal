from django.contrib import admin

from .models import SpecialWork
from purchased_items.models import PurchasedItem


# Register your models here.
# admin.site.register(SpecialWork)


class PurchasedItemInline(admin.TabularInline):
    model = PurchasedItem


@admin.register(SpecialWork)
class SpecialWorkAdmin(admin.ModelAdmin):
    inlines = [
        PurchasedItemInline,
    ]


# @admin.register(SpecialWork)
# class SpecialWork(admin.ModelAdmin):
#     list_display = (
#         'number',
#         'created',
#         'PurchaseRequest',
#         'executive_company',
#         'output_number',
#         'intput_number',
#         'conclusion',
#         'protocol',
#         'prescription',
#         ),
