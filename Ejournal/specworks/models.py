from django.utils.timezone import now as current_date
from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    TextField,
    DateField,
    ForeignKey,
    SET_NULL,
)

from purchase_requests.models import Request


# Create your models here.
class SpecialWork(Model):
    id = AutoField(
        primary_key=True,
    )
    number = CharField(
        max_length=255,
        # verbose_name='Number',
        verbose_name='Номер'
    )
    created = DateField(
        # auto_now_add=True,
        # verbose_name='Created',
        verbose_name='Дата',
        default=current_date,
        null=True,
    )
    request = ForeignKey(
        Request,
        on_delete=SET_NULL,
        # verbose_name='Request',
        verbose_name='Заявка',
        blank=True,
        null=True,
    )
    # request = CharField(
    #     max_length=255,
    #     # verbose_name='Request',
    #     verbose_name='Заявка',
    #     blank=True,
    # )
    # purchased_item = ForeignKey(
    #     PurchasedItem,
    #     on_delete=SET_NULL,
    #     blank=True,
    #     null=True,
    # )
    executive_company = CharField(
        max_length=50,
        # verbose_name='Executive company',
        verbose_name='Контрагент',
        blank=True,
    )
    output_number = CharField(
        max_length=50,
        # verbose_name='Output number',
        verbose_name='Исходящий №',
        blank=True,
    )
    intput_number = CharField(
        max_length=50,
        # verbose_name='Intput number',
        verbose_name='Входящий №',
        blank=True,
    )
    conclusion = CharField(
        max_length=50,
        # verbose_name='Conclusion',
        verbose_name='Заключение',
        blank=True,
    )
    protocol = CharField(
        max_length=50,
        # verbose_name='Protocol',
        verbose_name='Протокол',
        blank=True,
    )
    prescription = CharField(
        max_length=50,
        # verbose_name='Prescription',
        verbose_name='Предписание',
        blank=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'спецработы'
        verbose_name_plural = 'спецработы'
        # verbose_name_plural = 'Special works'

    def __str__(self):
        return f'{self.number} - {self.created}'

    def get_absolute_url(self):
        return reverse('edit_specwork', args=(self.id,))
