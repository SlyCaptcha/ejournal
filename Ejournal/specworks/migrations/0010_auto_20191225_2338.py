# Generated by Django 2.2.1 on 2019-12-25 20:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('specworks', '0009_remove_specialwork_purchased_item'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialwork',
            name='request',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='purchase_requests.Request', verbose_name='Заявка'),
        ),
    ]
