# Generated by Django 2.2.1 on 2019-12-25 18:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchased_items', '0004_remove_purchaseditem_special_works'),
        ('specworks', '0005_auto_20191225_2109'),
    ]

    operations = [
        migrations.AddField(
            model_name='specialwork',
            name='purchased_item',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='purchased_items.PurchasedItem'),
        ),
    ]
