from django.apps import AppConfig


class SpecworksConfig(AppConfig):
    name = 'specworks'
    verbose_name = 'Спецработы'
