from django.contrib import admin

from .models import ClosingDoc

# Register your models here.
admin.site.register(ClosingDoc)
