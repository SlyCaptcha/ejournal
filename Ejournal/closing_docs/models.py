from django.utils.timezone import now as current_date
from django.db import models
from django.db.models import (
    Model,
    AutoField,
    CharField,
    DecimalField,
    DateField,
    ForeignKey,
    SET_NULL,
)

from income_orders.models import IncomeOrder
from invoices.models import Invoice


# Create your models here.
class ClosingDoc(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    number = CharField(
        verbose_name='Номер документа',
        # verbose_name='Number',
        max_length=255,
        blank=True,
        null=True,
    )
    date = DateField(
        # auto_now_add=True,
        # verbose_name='Date',
        verbose_name='Дата документа',
        default=current_date,
        null=True,
    )
    amount = DecimalField(
        # verbose_name='Amount',
        verbose_name='Сумма',
        max_digits=10,
        decimal_places=2,
        default=0.00,
        blank=True,
        help_text='',
    )
    invoice = ForeignKey(
        Invoice,
        on_delete=SET_NULL,
        # verbose_name='Invoice',
        verbose_name='Счёт',
        null=True,
        blank=True,
    )
    income_order = ForeignKey(
        IncomeOrder,
        on_delete=SET_NULL,
        # verbose_name='Income Order',
        verbose_name='Приходный ордер',
        null=True,
        blank=True,
    )
    comment = CharField(
        verbose_name='Комментарий',
        # verbose_name='Comment',
        max_length=255,
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'закрывающий документ'
        verbose_name_plural = 'закрывающие документы'

    def __str__(self):
        return f'№{self.number} от {self.date}'

    def get_absolute_url(self):
        return reverse('edit_closing_doc', args=(self.id,))
