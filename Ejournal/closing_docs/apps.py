from django.apps import AppConfig


class ClosingDocsConfig(AppConfig):
    name = 'closing_docs'
    verbose_name = 'Закрывающие документы'
