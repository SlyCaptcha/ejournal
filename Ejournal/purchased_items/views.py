from django.http import HttpResponseNotFound, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect

from django.views.generic import ListView, DetailView

from .models import (
    PurchasedItem,
)
from .forms import (
    PurchasedItemForm,
)


# Purchase component
class PurchasedItemListView(ListView):
    model = PurchasedItem
    template_name = 'purchased_items.html'


@login_required(login_url='/accounts/login/')
def new_purchased_item(request):
    if request.method == "POST":
        form = PurchasedItemForm(request.POST)
        if form.is_valid():
            new_record = form.save(commit=False)
            new_record.save()
            return redirect('purchased_items')
    else:
        form = PurchasedItemForm()
    return render(request, 'form.html', {'form': form})


@login_required(login_url='/accounts/login/')
def edit_purchased_item(request, id):
    editable_record = get_object_or_404(PurchasedItem, id=id)
    if request.method == 'POST':
        form = PurchasedItemForm(request.POST, instance=editable_record)
        if form.is_valid():
            purchased_item = form.save(commit=False)
            purchased_item.save()
            return redirect('purchased_items')
    else:
        form = PurchasedItemForm(instance=editable_record)
    return render(request, 'form.html', {'form': form})


# @login_required(login_url='/accounts/login/')
# def purchase_delete(PurchaseRequest_number):
#     try:
#         purchase = Request.objects.get(PurchaseRequest_number=PurchaseRequest_number)
#         purchase.delete()
#         return redirect('/purchase_requests/')
#     except Request.DoesNotExist:
#         return HttpResponseNotFound('<h2>Request is not found.</h2>')
