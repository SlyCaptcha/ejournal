from django.contrib import admin

from .models import (
    PurchasedItem,
    TypeOfPurchasedItem,
    TypeOfQualityControl,
)


# Register your models here.
# admin.site.register(PurchasedItem)
# admin.site.register(TypeOfPurchasedItem)
# admin.site.register(TypeOfQualityControl)


@admin.register(TypeOfPurchasedItem)
class TypeOfPurchasedItemAdmin(admin.ModelAdmin):
    pass


@admin.register(TypeOfQualityControl)
class TypeOfQualityControlAdmin(admin.ModelAdmin):
    pass


@admin.register(PurchasedItem)
class PurchasedItemAdmin(admin.ModelAdmin):
    pass


# @admin.register(TypeOfPurchasedItem)
# class TypeOfPurchasedItemAdmin(admin.ModelAdmin):
#     list_display = ('name',)


# @admin.register(PurchasedItem)
# class PurchasedItemAdmin(admin.ModelAdmin):
#     fields = (
#         'type',
#         'title',
#         'description',
#         'count',
#         'serial_number',
#         # 'invoice_number',
#         'date_of_receipt',
#         'waybill_number',
#         'waybill_date',
#         'repair',
#         #'invoice_for_internal_displacement',
#         # 'incoming_inspection_act',
#         'special_works',
#     )
#     list_display = (
#         'type',
#         'title',
#         'description',
#         'count',
#         'serial_number',
#         # 'invoice_number',
#         'date_of_receipt',
#         'waybill_number',
#         'waybill_date',
#         'repair',
#         #'invoice_for_internal_displacement',
#         # 'incoming_inspection_act',
#         'special_works',
#     )
