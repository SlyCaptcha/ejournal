# Generated by Django 2.2.1 on 2019-12-21 16:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('purchased_items', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchaseditem',
            name='item_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='purchased_items.TypeOfPurchasedItem', verbose_name='Тип'),
        ),
        migrations.AlterField(
            model_name='typeofpurchaseditem',
            name='name',
            field=models.CharField(max_length=255, unique=True, verbose_name='Наименование'),
        ),
        migrations.AlterField(
            model_name='typeofqualitycontrol',
            name='number',
            field=models.CharField(max_length=1, unique=True, verbose_name='Номер'),
        ),
    ]
