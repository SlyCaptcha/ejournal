# Generated by Django 2.2.1 on 2019-12-25 20:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchased_items', '0005_purchaseditem_special_works'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchaseditem',
            name='special_works',
        ),
    ]
