# Generated by Django 2.2.1 on 2019-12-25 20:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('specworks', '0007_remove_specialwork_purchased_item'),
        ('purchased_items', '0004_remove_purchaseditem_special_works'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaseditem',
            name='special_works',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='specworks.SpecialWork', verbose_name='Спецработы'),
        ),
    ]
