from django.contrib.auth.models import User
from django.forms import (
    ModelForm,
    CharField,
    IntegerField,
    FloatField,
    ModelChoiceField,
    ModelMultipleChoiceField,
    DateField,
    BooleanField,

    # windgets
    DateInput,
    TextInput,
    Textarea,
    Select,
    SelectMultiple,
    SelectDateWidget,
)
from datetime import datetime

from .models import (
    # TypeOfPurchasedItem,
    PurchasedItem,
)
# from invoices.models import Invoice
# from repairs.models import Repair
# from specworks.models import SpecialWork


class PurchasedItemForm(ModelForm):
    class Meta:
        model = PurchasedItem
        fields = '__all__'

    # type = ModelChoiceField(
    #     queryset=TypeOfPurchasedItem.objects.all(),
    #     # label='type',
    #     label='Категория',
    #     widget=Select(),
    #     required=True,
    # )
    # title = CharField(
    #     label='Наименование',
    #     # label='Title',
    #     max_length=100,
    #     required=True,
    # )
    # description = CharField(
    #     label='Описание',
    #     # label='Description',
    #     max_length=100,
    #     required=False,
    # )
    # count = FloatField(
    #     label='Количество',
    #     required=False,
    #     initial=1.00,
    # )
    # # count = IntegerField(
    # #     label='Количество',
    # #     required=False,
    # #     initial=1,
    # # )
    # serial_number = CharField(
    #     label='Серийный номер',
    #     # label='Serial number',
    #     widget=Textarea(),
    #     required=False,
    # )
    # invoice_number = ModelChoiceField(
    #     queryset=Invoice.objects.all(),
    #     label='Номер счета',
    #     # label='Invoice number',
    #     required=False,
    # )
    # date_of_receipt = DateField(
    #     label='Получено',
    #     # label='Date of receipt',
    #     required=False,
    #     widget=SelectDateWidget(),
    #     initial=datetime.now().date(),
    # )
    # waybill_number = CharField(
    #     label='Номер товарной накладной (УПД)',
    #     # label='Waybill number',
    #     max_length=50,
    #     required=False,
    # )
    # waybill_date = DateField(
    #     label='Дата товарной налкадной (УПД)',
    #     # label='Waybill date',
    #     required=False,
    # )
    # repair = ModelChoiceField(
    #     queryset=Repair.objects.all(),
    #     required=False,
    #     label='Ремонт',
    #     # label='Repair',
    # )
    # # invoice_for_internal_displacement = ModelChoiceField(
    # #     queryset=InvoiceForInternalDisplacement.objects.all(),
    # #     required=False,
    # #     label='Накладная на внутреннее перемещение',
    # #     # label='Invoice for internal displacement',
    # # )
    # # incoming_inspection_act = ModelChoiceField(
    # #     queryset=IncomingInspectionAct.objects.all(),
    # #     required=False,
    # #     label='Акт входного контроля',
    # #     # label='Incoming inspection act',
    # # )
    # special_works = ModelChoiceField(
    #     queryset=SpecialWork.objects.all(),
    #     required=False,
    #     label='Спецработы',
    #     # label='Special works',
    # )
