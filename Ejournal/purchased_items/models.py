from django.urls import reverse
from django.db.models import (
    Model,
    AutoField,
    CharField,
    BooleanField,
    FloatField,
    ForeignKey,
    SET_NULL,
)

from invoices.models import Invoice
from specworks.models import SpecialWork


# Create your models here.
# Категория покупого комлектующего изделия (ПКИ)
class TypeOfPurchasedItem(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
        # default=uuid4
    )
    number = CharField(
        # 'Code number',
        'Номер',
        max_length=2,
        unique=True,
        # error_messages={
        #     'blank': 'Введите уникальный номер типа ПКИ!',
        # }
    )
    name = CharField(
        'Наименование',
        # verbose_name='Name',
        max_length=255,
        unique=True,
        help_text='',
        # error_messages={
        #     'blank': 'Введите уникальное наименование!',
        # }
    )

    class Meta:
        ordering = ['id', ]
        # verbose_name_plural = 'Purchase component categories'
        verbose_name = 'тип покупного комлектующего изделия'
        verbose_name_plural = 'типы покупных комлектующих изделий'

    def __str__(self):
        return f'{self.number}-{self.name}'


# Тип приемки
class TypeOfQualityControl(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    number = CharField(
        # 'Code number',
        'Номер',
        max_length=1,
        unique=True,
        # error_messages={
        #     'blank': 'Введите уникальный номер типа приемки!',
        # },
    )
    title = CharField(
        # verbose_name='Title',
        verbose_name='Наименование',
        max_length=30,
        unique=True,
        # error_messages={
        #     'blank': 'Ввведите уникальное наименование типа приемки!',
        # },
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'тип приёмки'
        verbose_name_plural = 'типы приёмок'

    def __str__(self):
        return f'{self.number} - {self.title}'


# Покупные комплектующие изделия
class PurchasedItem(Model):
    id = AutoField(
        primary_key=True,
        max_length=3,
    )
    item_type = ForeignKey(
        'TypeOfPurchasedItem',
        on_delete=SET_NULL,
        # verbose_name='type',
        verbose_name='Тип',
        blank=True,
        null=True,
        help_text='',
    )
    title = CharField(
        'Наименование',
        # verbose_name='Title',
        max_length=100,
        help_text='',
    )
    count = FloatField(
        verbose_name='Количество',
        blank=True,
        default=1.00,
    )
    serial_number = CharField(
        # verbose_name='Serial number',
        verbose_name='Серийный номер',
        max_length=255,
        blank=True,
        help_text='',
    )
    invoice = ForeignKey(
        Invoice,
        on_delete=SET_NULL,
        # verbose_name='Invoice',
        verbose_name='Счёт',
        blank=True,
        null=True,
    )
    type_of_quality_control = ForeignKey(
        'TypeOfQualityControl',
        on_delete=SET_NULL,
        # verbose_name='Type of quality control',
        verbose_name='Тип приемки',
        blank=True,
        null=True,
    )
    special_check = BooleanField(
        verbose_name='Специальные проверки (СП)',
        default=False,
        blank=True,
    )
    special_studies = BooleanField(
        verbose_name='Специальные исследования (СИ)',
        default=False,
        blank=True,
    )
    special_works = ForeignKey(
        SpecialWork,
        on_delete=SET_NULL,
        # verbose_name='Special works',
        verbose_name='Спецработы',
        blank=True,
        null=True,
        help_text='',
    )

    class Meta:
        ordering = ['id', ]
        verbose_name = 'покупное комплектующее изделие'
        verbose_name_plural = 'покупные комплектующие изделия'

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse('edit_purchased_item', args=(self.id,))
