from django.apps import AppConfig


class PurchasedItemsConfig(AppConfig):
    name = 'purchased_items'
    verbose_name = 'ПКИ'
